# 設定頁面

有關應用程式整體簡介,請參考文章列表(首頁)的說明內容.

## 功能和操作

設定這邊有兩種功能

* 通知,有關這個功能,其實他也是展示用的,由於沒有伺服器,沒有帳號系統,所以比較難設計使用的情境,不過又想把他放進去,就產生一個怪異的選項,開啟通知後,下方通知時間會跑出來,關閉則會隱藏,點選通知時間會可以選擇一個時間,每天的那個時間就會顯示通知,通知可以選擇開啟這個應用,這邊只是想簡單展示一些東西.
* 套用主題,在這個應用做好了五種主題顏色,你可以選擇想要的並且啟動他.選擇和目前不同的主題就會顯示應用的按鈕.按下即可換主題.

點擊關於可以看見說明資訊,由於豎屏橫屏並沒有用不同UI,所以只用一張附圖

<img src="../zh/images/setting.jpg" class="portrait"/>

## 頁面展示重點

* 最一開始要展示的本來是PreferenceFragment的用法,不過從layout自訂到Preference自訂,已經有點看不出他原來的便利性了.這邊主要也是展示他能給的彈性.不過自訂太多可能直接用Fragment或許還比較方便.
* 通知這邊主要是WorkManager和Notification,目前長時間背景執行推薦是使用WorkManager,android這方面的工具也不斷在變化,最新的來到WorkManager,而Notification這邊也是不斷改變,功能也漸趨複雜化,這邊只有做簡單的通知.
* 主題改變這邊,主要的主題使用的是Material3的新版本,擁有更多的顏色需要設定,這邊是利用[figma plugin](https://goo.gle/material-theme-builder-figma)產生這些theme,也是目前官網提供的工具.跟以前的web版工具比起來不怎麼好用.Material3另一個特別的是dynamic colors,原本以為他能帶來更多修改theme的便利,但是實際上他還是放置在application/activity creation,至於fragment部分還是要用context,所以我還是沿用我自己的library來做主題修改.
* 其他有關架構,或非UI的細節部分,請至[原始碼](https://bitbucket.org/bjchen/small-note/)查看



