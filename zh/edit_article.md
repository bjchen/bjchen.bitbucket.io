# 編輯文章頁面

有關應用程式整體簡介,請參考文章列表(首頁)的說明內容.

## 功能和操作

編輯文章內容

### 新增文章

新增文章亦使用這個頁面,不過沒有刪除文章的選項

### 編輯文章

編輯文章主要操作有包含

* 編輯標題,不能為空,否則無法寫入
* 編輯內文,不能為空,否則無法寫入
* 是否加密文章,加密文章切換到解密時,儲存會需要認證,刪除加密文章也需要認證
* 選取背景圖,會給一張默認圖,可以選擇想要的圖片.

另外點擊設定可進入設定頁面,點擊關於可以看見說明資訊,下圖為豎屏模式頁面

<img src="../zh/images/edit_portrait.jpg" class="portrait"/>

下圖為橫屏模式頁面

<img src="../zh/images/edit_landscape.jpg" class="landscape"/>



## 頁面展示重點

* 豎屏模式下,版面root為CoordinatorLayout,這邊使用他是為了搭配CollapsingToolbarLayout,CoordinatorLayout捲動時可以和他有互動.在豎屏模式下,文章列表,列表頁面和編輯頁面都有類似的設計.除此之外還是以ConstraintLayout為主
* 但是CollapsingToolbarLayout本身的標題是無法編輯的,我們想要編輯標題,所以這邊利用MotionLayout,來操作一個TextInputLayout的大小改變,模擬出CollapsingLayout裡面的標題改變大小風格.
* 由於背景圖的關係,可能會遇到前方字體顯示不清的狀況,我利用圖片api提供的一個顏色值來運算,計算他的luminance,然後決定使用白色或是黑色的字.也使用一個半透明圖層讓圖片和文字的對比能更強烈.
* 加密文章的按鈕使用方形和方形凹槽,這邊是使用Material design範例的寫法來改變,詳情可以看[原始碼](https://bitbucket.org/bjchen/small-note/)
* 橫屏模式下使用rail當作左選單
* 橫屏模式下,背景圖的顯示在左半,文字在右半,另外提供一個將圖片放大的按鈕,你可以看看圖片全屏下的顯示,這邊也是利用MotionLayout達成.
* 其他有關架構,或非UI的細節部分,請至[原始碼](https://bitbucket.org/bjchen/small-note/)查看

